String macString(const unsigned char *mac) {

  char buf[20];
  snprintf(buf, sizeof(buf), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
  
  return String(buf);
}

String tsToDate(time_t ts) {
  tm *timeStructure = localtime(&ts);
  char date[22];
  
  strftime(date, 22, "%d. %m. %Y %H:%M:%S", timeStructure);
  return String(date);
}

bool orderMacEntries(MacEntry a, MacEntry b) {
  if (a.address != b.address) {
    return (a.address < b.address);
  }
  return (a.timestamp < b.timestamp);
}

MacStatsEntry createStatsEntry(String address, std::vector<int> durations) {

  int timesSeen = durations.size();
  int duration = std::accumulate(durations.begin(), durations.end(), 0);
  int avgDuration = duration / timesSeen;
  
  return MacStatsEntry{address, duration, avgDuration, timesSeen};
}

std::vector<MacStatsEntry> processTimestampGroups(std::vector<String> addresses, std::vector<std::vector<time_t>> groups){

  std::vector<MacStatsEntry> entries;
  for (auto group = groups.begin(); group != groups.end(); ++group) {

    time_t firstTimeStamp = (*group)[0];
    time_t lastTimeStamp = firstTimeStamp;
    std::vector<int> durations;

    for (auto ts = group->begin(); ts != group->end(); ++ts) {

      if (*ts - lastTimeStamp > 60) {
        durations.push_back(lastTimeStamp - firstTimeStamp);
        firstTimeStamp = *ts;
      }

      lastTimeStamp = *ts;
    }
    durations.push_back(lastTimeStamp - firstTimeStamp);

    String address = addresses[std::distance(groups.begin(), group)];
    entries.push_back( createStatsEntry(address, durations) );
  }

  return entries;
}

std::vector<MacStatsEntry> processStats(std::vector<MacEntry> entries) {

  if (entries.empty()) {
    return std::vector<MacStatsEntry>();
  }
  
  std::sort(entries.begin(), entries.end(), orderMacEntries);

  String lastAddress = entries[0].address;
  std::vector<std::vector<time_t>> groups;
  std::vector<time_t> timestamps;
  std::vector<String> addresses;

  addresses.push_back(lastAddress);

  for (auto i = entries.begin(); i != entries.end(); ++i) {
    if (lastAddress == i->address) {
      
      timestamps.push_back(i->timestamp);
    } else {
      
      lastAddress = i->address;
      
      addresses.push_back(lastAddress);
      groups.push_back(timestamps);
      
      timestamps.clear();
      timestamps.push_back(i->timestamp);
    }
  }
  groups.push_back(timestamps);

  return processTimestampGroups(addresses, groups);
}
