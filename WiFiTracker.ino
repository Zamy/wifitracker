#include <ESP8266WiFi.h>
#include <time.h>
#include <Ticker.h>
#include <ESP8266WebServer.h>
#include <vector>
#include <sstream>
#include <FS.h>
#define BUILTIN_LED 2


struct MacEntry {
  String address;
  time_t timestamp;
};

struct MacStatsEntry {
  String address;
  int durationOverall;
  int durationAverage;
  int timesSeen;
};

int maxProbeDelay = 60;
String apSSID = "ESP";
String apPass = "12345678";
String staSSID = "SSID";
String staPass = "PASS";

IPAddress apIP(192, 168, 4, 1);
IPAddress apGateway(192, 168, 4, 1);
IPAddress apSubnet(255, 255, 255, 0);

time_t now = time(nullptr);
bool timeSynced = false;

std::vector<MacEntry> currentMacs;

Ticker saveTimer;

ESP8266WebServer server(80);

void setup() {
  Serial.begin(115200);
  SPIFFS.begin();
  pinMode(BUILTIN_LED, OUTPUT);

  resetFile();

  configTime(2 * 3600, 0, "pool.ntp.org", "time.nist.gov");
  
  setupWiFi();
  syncTime();
  createAP();
  setupServer();
  startMonitoring();

  saveTimer.attach(10, saveMacs);
}

void loop() {

  now = time(nullptr);
  
  server.handleClient();
}


void setupWiFi() {
  WiFi.setAutoConnect(0);
  WiFi.mode(WIFI_AP_STA);
  WiFi.begin(staSSID, staPass);
}

void syncTime() {
  Serial.println(WiFi.waitForConnectResult());
  Serial.println("syncing time");
  while(time(nullptr) <= 1000000000) {
    delay(100);
  }
  Serial.println("Time synced");
}

void createAP() {
  //WiFi.disconnect(true);
  WiFi.softAPConfig(apIP, apGateway, apSubnet);
  WiFi.softAP(apSSID, apPass);
}
