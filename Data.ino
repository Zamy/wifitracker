
void saveMacs() {

  if (currentMacs.empty()) {
    return;
  }

  std::vector<MacEntry> data = currentMacs;
  currentMacs.clear();

  File file = SPIFFS.open("data.csv", "a");

  for (auto &i: data){

    file.print(i.address);
    file.print(',');
    file.print(i.timestamp);
    file.print("\n");
  }

  file.close();
  Serial.println("Data saved");

}

std::vector<MacEntry> loadMacs() {

  File file = SPIFFS.open("data.csv", "r");


  std::vector<MacEntry> result;

  while (file.peek() != -1) {
    String mac = file.readStringUntil(',');
    String ts = file.readStringUntil('\n');
    
    time_t timestamp = (time_t) std::strtol(ts.c_str(), nullptr, 10); 
    MacEntry entry = {mac, timestamp};
    result.push_back(entry);
    //Serial.printf("%s: %s\n", mac.c_str(), ts.c_str());
  }
  //result.push_back(mac);
  
  return result;
}

void resetFile() {
  SPIFFS.open("data.csv", "w").close();
}

String loadHTML(String fileName) {

  File file = SPIFFS.open(fileName, "r");

  String html = file.readString();

  file.close();
  return html;
}
