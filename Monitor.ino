
WiFiEventHandler probeRequstReceived;

MacEntry currEntry;


void startMonitoring(){
  probeRequstReceived = WiFi.onSoftAPModeProbeRequestReceived(&onProbeRequest);
}

void onProbeRequest(const WiFiEventSoftAPModeProbeRequestReceived& event) {

  MacEntry lastEntry = currEntry;
  
  currEntry.address = macString(event.mac);
  currEntry.timestamp = now;

  if (lastEntry.address == currEntry.address && lastEntry.timestamp == currEntry.timestamp) {
    return;
  }

  Serial.println(currEntry.address);

  currentMacs.push_back(currEntry);
}
