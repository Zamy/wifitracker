
void setupServer() {

  server.on("/", showList);
  server.on("/stats", showStats);
  
  server.begin();
  Serial.println("Server started");
}

String generateListTable(std::vector<MacEntry> macs) {
  Serial.println("generatin table");
  std::ostringstream oss;
  for (auto &entry: macs) {
    oss << "<tr><td>"
        << entry.address.c_str()
        << "</td><td>"
        << tsToDate(entry.timestamp).c_str()
        << "</td></tr>";
  }
  Serial.println("table generated");
  return String(oss.str().c_str());
}


void showList() {
  String html = loadHTML("/index.html");
  String generatedTable = generateListTable( loadMacs() );
  
  size_t buf_size = snprintf(nullptr, 0, html.c_str(), generatedTable.c_str());
  std::unique_ptr<char[]> buf(new char[buf_size]);
  snprintf(buf.get(), buf_size, html.c_str(), generatedTable.c_str());
  Serial.println("Sending table");
  server.send(200, "text/html", String(buf.get()));
  
}

String generateStatsTable(std::vector<MacStatsEntry> entries) {

  std::ostringstream oss;
  for (auto &entry: entries) {
    oss << "<tr><td>"
        << entry.address.c_str()
        << "</td><td>"
        << entry.durationOverall
        << "</td><td>"
        << entry.durationAverage
        << "</td><td>"
        << entry.timesSeen
        << "</td></tr>";
  }

  return String(oss.str().c_str());
}


void showStats() {
  std::vector<MacEntry> macs = loadMacs();
  std::vector<MacStatsEntry> stats = processStats(macs);
  
  String html = loadHTML("/stats.html");
  String statsTable = generateStatsTable(stats);

  size_t buf_size = snprintf(nullptr, 0, html.c_str(), statsTable.c_str());
  std::unique_ptr<char[]> buf(new char[buf_size]);
  snprintf(buf.get(), buf_size, html.c_str(), statsTable.c_str());

  server.send(200, "text/html", String(buf.get()));
}
